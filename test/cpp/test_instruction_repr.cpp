#include "catch_amalgamated.hpp"

#include "mallard/spec/statement.hpp"

#include <sstream>
#include <tuple>

using mallard::spec::Array;
using mallard::spec::Statement;
using std::get;
using std::make_tuple;
using std::stringstream;

TEST_CASE("repr_terminal", "[repr]") {
    auto test_data = GENERATE(
        make_tuple("var_name", "variable", Statement::VARIABLE),
        make_tuple("314159", "int_literal", Statement::INT_LITERAL)
    );
    auto statement = Statement {
        .terminal = get<0>(test_data),
        .id = get<2>(test_data)
    };

    stringstream buffer, expected;

    expected << "Statement(id=" << statement.id 
        << ", " << get<1>(test_data) << "=" << statement.terminal 
        << ")";
    statement.repr(buffer);

    CHECK(buffer.str() == expected.str());
}

TEST_CASE("repr_productions", "[repr]") {
    Statement single_statements[] = {
        {
            .terminal = "271828",
            .id = Statement::INT_LITERAL
        },
        {
            .terminal = "var_y",
            .id = Statement::VARIABLE
        },
        {
            .productions = Array<Statement> {
                .ptr = nullptr,
                .count = 0
            },
            .id = Statement::LIST_LITERAL
        }
    };
    auto test_data = GENERATE(
        make_tuple("list_literal", Statement::LIST_LITERAL),
        make_tuple("set_literal", Statement::SET_LITERAL)
    );
    auto statement = Statement {
        .productions = Array<Statement> {
            .ptr = single_statements,
            .count = 3
        },
        .id = get<1>(test_data)
    };

    stringstream buffer, expected;

    expected << "Statement(id=" << statement.id 
        << ", " << get<0>(test_data) << "=[Statement(id=1, int_literal=271828), Statement(id=0, variable=var_y), Statement(id=2, list_literal=[])])";
    statement.repr(buffer);

    CHECK(buffer.str() == expected.str());
}

TEST_CASE("repr_production_pairs", "[repr]") {
    Statement single_statements[] = {
        {
            .terminal = "var_z",
            .id = Statement::VARIABLE
        },
        {
            .productions = Array<Statement> {
                .ptr = nullptr,
                .count = 0
            },
            .id = Statement::LIST_LITERAL
        },
        {
            .productions = Array<Statement> {
                .ptr = nullptr,
                .count = 0
            },
            .id = Statement::MAP_LITERAL
        },
        {
            .terminal = "602214",
            .id = Statement::INT_LITERAL
        }
    };
    auto test_data = GENERATE(
        make_tuple("map_literal", Statement::MAP_LITERAL)
    );

    auto statement = Statement {
        .productions = Array<Statement> {
            .ptr = single_statements,
            .count = 4
        },
        .id = get<1>(test_data)
    };

    stringstream buffer, expected;

    expected << "Statement(id=" << statement.id 
        << ", " << get<0>(test_data) << "=[(Statement(id=0, variable=var_z), Statement(id=2, list_literal=[])), (Statement(id=4, map_literal=[]), Statement(id=1, int_literal=602214))])";
        
    statement.repr(buffer);

    CHECK(buffer.str() == expected.str());
}

TEST_CASE("repr_binary_expr", "[repr]") {
    Statement base_statements[] = {
        {
            .terminal = "var_u",
            .id = Statement::VARIABLE
        },
        {
            .terminal = "1380649",
            .id = Statement::INT_LITERAL
        },
        {
            .terminal = "+",
            .productions = Array<Statement> {
                .ptr = base_statements,
                .count = 2
            },
            .id = Statement::BINARY_EXPRESSION
        },
        {
            .terminal = "var_v",
            .id = Statement::VARIABLE
        }
    };
    auto statement = Statement {
        .terminal = "/",
        .productions = Array<Statement> {
            .ptr = base_statements + 2,
            .count = 2
        },
        .id = Statement::BINARY_EXPRESSION
    };

    stringstream buffer, expected;
    expected << "Statement(id=5, left=Statement(id=5, left=Statement(id=0, variable=var_u), symbol=+, right=Statement(id=1, int_literal=1380649)), symbol=/, right=Statement(id=0, variable=var_v))";
    statement.repr(buffer);

    CHECK(buffer.str() == expected.str());
}

TEST_CASE("repr_anonymous_function", "[repr]") {
    const char* terminals[] = { "x", "y"};
    Statement fn_statements[] = {
        {
            .terminal = "x",
            .id = Statement::VARIABLE
        }
    };
    auto statement = Statement {
        .terminals = Array<const char*> {
            .ptr = terminals,
            .count = 2
        },
        .productions = Array<Statement> {
            .ptr = fn_statements,
            .count = 1
        },
        .id = Statement::ANONYMOUS_FUNCTION
    };

    stringstream buffer, expected;
    expected << "Statement(id=6, parameters=[x, y], statements=[Statement(id=0, variable=x)])";
    statement.repr(buffer);

    CHECK(buffer.str() == expected.str());
}

TEST_CASE("repr_function_application", "[repr]") {
    const char* terminals[] = { "x", "y"};
    Statement statements[] = {
        {
            .terminal = "p",
            .id = Statement::VARIABLE
        },
        {
            .terminal = "q",
            .id = Statement::VARIABLE
        },
        {
            .productions = Array<Statement> {
                .ptr = statements,
                .count = 2
            },
            .id = Statement::FUNCTION_APPLICATION
        },
        {
            .terminal = "p",
            .id = Statement::VARIABLE
        }
    };
    auto statement = Statement {
        .productions = Array<Statement> {
            .ptr = statements + 2,
            .count = 2
        },
        .id = Statement::FUNCTION_APPLICATION
    };

    stringstream buffer, expected;
    expected << "Statement(id=7, function=Statement(id=7, function=Statement(id=0, variable=p), arguments=[Statement(id=0, variable=q)]), arguments=[Statement(id=0, variable=p)])";
    statement.repr(buffer);

    CHECK(buffer.str() == expected.str());
}