include $(VOLAUVENT_HOME)/application.mk

TEST_DEPS := $(src_APP_OUTPUT)
TEST_INC_DIRS := $(src_INC_DIRS)
TEST_LIB_DIRS := $(src_OUTPUT_DIR) 
TEST_STATIC_LIB := mallard_spec

$(eval $(call define_app_rules,test,mallard_spooky_engine_test,$(TEST_DEPS),$(TEST_INC_DIRS),$(TEST_LIB_DIRS),$(TEST_STATIC_LIB)))

