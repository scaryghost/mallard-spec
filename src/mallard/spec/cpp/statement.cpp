#include "mallard/spec/statement.hpp"

using std::get;
using std::make_tuple;
using std::ostream;
using std::tuple;

namespace mallard {
namespace spec {

template <class T>
ostream& operator<<(ostream& os, const Array<T>& values) {
    os << "[";
    if (values.count > 0) {
        auto pos = 0;

        os << values[pos];
        for(pos = 1; pos < values.count; pos++) {
            os << ", " << values[pos];
        }
    }
    os << "]";
   
    return os;
}

template <>
ostream& operator<<(ostream& os, const Array<Statement>& values) {
    os << "[";
    if (values.count > 0) {
        auto pos = 0;

        values[pos].repr(os);
        for(pos = 1; pos < values.count; pos++) {
            values[pos].repr(os << ", ");
        }
    }
    os << "]";
   
    return os;
}

ostream& operator<<(ostream& os, const tuple<Statement*, Statement*>& entry) {
    return get<1>(entry)->repr(
        get<0>(entry)->repr(os << "(") << ", "
    ) << ")";
}

ostream& Statement::repr(ostream& os) const {
    os << "Statement(id=" << id << ", ";

    switch(id) {
    case Statement::VARIABLE:
        os << "variable=" << terminal;
        break;
    case Statement::INT_LITERAL:
        os << "int_literal=" << terminal;
        break;
    case Statement::LIST_LITERAL:
        os << "list_literal=" << productions;
        break;
    case Statement::SET_LITERAL:
        os << "set_literal=" << productions;
        break;
    case Statement::MAP_LITERAL: {
        os << "map_literal=[";
        if (productions.count > 0) {
            auto pos = 0;

            os << make_tuple(productions.ptr, productions.ptr + 1);
            for(auto pos = 2; pos < productions.count; pos += 2) {
                os << ", " << make_tuple(productions.ptr + pos, productions.ptr + pos + 1);
            }
        }
        os << "]";
        break;
    }
    case Statement::BINARY_EXPRESSION:
        productions[1].repr(
            productions[0].repr(os << "left=")
                << ", symbol=" << terminal
                << ", right="
        );
        break;
    case Statement::ANONYMOUS_FUNCTION:
        os << "parameters=" << terminals
            << ", statements=" << productions;
        break;
    case Statement::FUNCTION_APPLICATION: {
        auto sub_array = Array<Statement> {
            .ptr = productions.ptr + 1,
            .count = productions.count - 1
        };
        productions[0].repr(os << "function=") 
             << ", arguments=" << sub_array;
        break;
    }
    }

    os << ")";
    return os;

}

}
}
