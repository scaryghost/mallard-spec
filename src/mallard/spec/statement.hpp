#pragma once

#include <cstddef>
#include <ostream>
#include <tuple>

namespace mallard {
namespace spec {

template <class T>
struct Array {
    T* ptr;
    const std::size_t count;

    T& operator[](std::size_t pos) const {
        return ptr[pos];
    }
};

struct Statement {
    std::ostream& repr(std::ostream& os) const;

    union {
        const char* terminal;
        Array<const char*> terminals;
    };
    Array<Statement> productions;
    enum {
        VARIABLE = 0,
        INT_LITERAL,
        LIST_LITERAL,
        SET_LITERAL,
        MAP_LITERAL,
        BINARY_EXPRESSION,
        ANONYMOUS_FUNCTION,
        FUNCTION_APPLICATION
    } id;

};

}
}
