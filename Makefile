VOLAUVENT_HOME=buildSrc/volauvent
CXXFLAGS := -std=c++14

.PHONY: all clean

all: src-all
clean: src-clean test-clean
test: test-all
	exec $(test_APP_OUTPUT) $(ARGS)

include $(VOLAUVENT_HOME)/global_config.mk

include src/rules.mk
include test/rules.mk
